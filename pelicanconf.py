#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Христо Христов'
SITENAME = '9б клас'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Sofia'
DEFAULT_DATE_FORMAT = '%Y-%m-%d'
DEFAULT_LANG = 'bg'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Група от препратки, които се показват най-отдолу на index.html страницата, но за сега
# не се сещам за какво може да ни послужат.
# Blogroll
#LINKS = (('Pelican', 'https://getpelican.com/'),
#         ('Python.org', 'https://www.python.org/'),
#         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# А тези са от мен
# Използвай името на файла като заглавие на страницата
# FILENAME_METADATA = '(?P<title>.*)'
# Използвай името на бащината папка като име на категорията
USE_FOLDER_AS_CATEGORY = True
# Използвай датата от файла като дата на публикуването на статията
DEFAULT_DATE = 'fs'
THEME = '8b'
FILENAME_METADATA = '(?P<date>\d{4}-\d{2}-\d{2})\s(?P<title>.*)'
STATIC_PATHS = ['videos']
